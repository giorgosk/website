# Greek Drupal Community website
Developed with love.

Based on the [Drupal recommended project](https://github.com/drupal/recommended-project)

# Requirements
You can find the Drupal requirements at [Drupal minimum requirements](https://www.drupal.org/docs/system-requirements).

You will also need to have [composer](https://getcomposer.org/doc/00-intro.md) installed in your system.

# Setup
You will need a configured server and an SQL database.

- Copy the `.env.example` file from the project root directory over to the `.env` in the project root directory.
- Override any variable according to your installation.

# installation
From the project root, run
```
$ composer install
```
Then from the project root, run
```
./vendor/bin/drush --yes site:install --existing-config
```

# Contributions

## Content / Non code contributions 

Just help on the issue queue with filling issues or with feedback on existing ones

## Development contributions

### Setting up locally

From gitlab interface fork project to your own gitlab account

### clone fork locally
to get the code make sure you replace USERNAME appropriately
```
git clone git@gitlab.com:[USERNAME]/website.git mydrupal.gr
```

### create connection with upstream (base repo)
```
git remote add upstream git@gitlab.com:drupal-greece/website.git
```

## Run site locally

## connect local database

do one of the following 
- rename `.env.example` to `.env` and modify the credentials in the file
- in case of lando can rename `.env.example.lando` to `.env`
- create `web/sites/default/settings.local.php` from `web/sites/example.settings.local.php` and add your database credentials 

## development with lando 
One way of setting up locally is using Lando.

Requirements / setup 
- Docker https://docs.docker.com/get-docker/
- Lando https://lando.dev/download/ 

There is already a configured `.lando.yml` file in the root of the repo you can start it with

```
lando start
```
if you have setup local database (see below) and everything works correctly you can access website by visiting https://mydrupalgr.lndo.site

to stop the containers you can use
```
lando stop
```

to see info about your containers and how to access them
```
lando info
```

if you want to invoke drush put run it through lando i.e.
```
lando drush status
```

## Sample development workflow

Pull latest changes in your master
```
git checkout master
git pull upstream master
```
Do any changes required to solve problem or fix issue.


The website is exporting configuration in the `config/sync` directory in the project root. After updating the website,

```
./vendor/bin/drush cex
```
to export the configuration. Then commit the configuration.
*WARNING: DO NOT COMMIT ANY SENSITIVE DATA.*


commit your changes 
```
git add *
git commit -m '#1 Fixing issue one, solves all world problems'
``` 

push to your local repo
```
git push origin master
```

go to the gitlab interface and prepare a Merge Request to the upstream (Base) repo


## Contribute by reviewing a merge request locally 
Setup a development environment as described above.

Gitlab describes how to [get a merge request locally](https://docs.gitlab.com/ee/user/project/merge_requests/reviewing_and_managing_merge_requests.html#checkout-merge-requests-locally)

If merge request seems appropriate you can [approve it](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html)

Otherwise you can leave comments as merge request comments.

For more involved merge requests we might need more than 1 approval (1st one might approves, a 2nd one can just merge if he/she approves)

